import type { Config } from "@jest/types";

const config: Config.InitialOptions = {
    verbose: false,
    transform: {
        "^.+\\.ts$": "ts-jest",
    },
    testEnvironment: "node",
    testMatch: [
        "**/src/**/*.spec.ts",
        "**/src/**/*.test.ts",
    ],
    testPathIgnorePatterns: [
        "/node_modules/",
        "/integration-tests/",
    ],
    collectCoverage: false,
    collectCoverageFrom: [
        "src/**/*.ts",
        "!src/**/index.ts",
    ],
    moduleFileExtensions: [
        "js",
        "ts",
    ],
    moduleNameMapper: {
    },
};

export default config;