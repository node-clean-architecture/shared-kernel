import { UniqueId } from "../common";
import { IEntity } from "./IEntity";

export interface IDeletableEntity extends IEntity {
    deletedOn?: Date;
    deletedBy?: UniqueId;
}
