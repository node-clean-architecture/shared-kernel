import { UniqueId } from "../common";

export interface IEntity {
    id: UniqueId;
}
