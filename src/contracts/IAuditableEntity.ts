import { UniqueId } from "../common";
import { IEntity } from "./IEntity";

export interface IAuditableEntity extends IEntity {
    createdOn: Date;
    createdBy?: UniqueId;

    updatedOn?: Date;
    updatedBy?: UniqueId;
}
