export interface ICreateNewEntityParameters<TParams> {
    new: TParams;
    existing?: undefined;
}

export interface ICreateExistingEntityParameters<TParams> {
    new?: undefined;
    existing: TParams;
}

export type ICreateEntityParameters<TNewEntityParams, TExistingEntityParams> =
    ICreateNewEntityParameters<TNewEntityParams> |
    ICreateExistingEntityParameters<TExistingEntityParams>;
