export class InvalidArgumentError extends Error {
    constructor(message: string) {
        super(message);

        this.name = InvalidArgumentError.name;

        Error.captureStackTrace(this, InvalidArgumentError);
    }

    static instanceOf(err: Error): err is InvalidArgumentError {
        return err.name === InvalidArgumentError.name;
    }
}