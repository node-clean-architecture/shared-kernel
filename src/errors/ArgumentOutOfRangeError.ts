export class ArgumentOutOfRangeError extends Error {
    constructor(value: number, min?: number, max?: number) {
        super(`Expected ${value} to be between ${min} and ${max}`);

        this.name = ArgumentOutOfRangeError.name;

        Error.captureStackTrace(this, ArgumentOutOfRangeError);
    }

    static instanceOf(err: Error): err is ArgumentOutOfRangeError {
        return err.name === ArgumentOutOfRangeError.name;
    }
}