import { DomainEvent, UniqueId } from "./common";
import { IAuditableEntity, IDeletableEntity, IEntity } from "./contracts";
import { BusinessRuleValidationError, IBusinessRule, IBusinessRuleAsync } from "./rules";

export abstract class Entity implements IEntity, IAuditableEntity, IDeletableEntity {
    readonly createdOn = new Date();
    createdBy?: UniqueId;

    updatedOn?: Date;
    updatedBy?: UniqueId;

    deletedOn?: Date;
    deletedBy?: UniqueId;

    private _id = new UniqueId();
    protected _domainEvents: DomainEvent[] = [];

    get id(): UniqueId {
        return this._id;
    }

    protected set id(value: UniqueId) {
        this._id = value;
    }

    get domainEvents(): ReadonlyArray<DomainEvent> {
        return this._domainEvents.slice();
    }

    protected addDomainEvent(newEvent: DomainEvent): void {
        this._domainEvents.push(newEvent);
    }

    protected clearDomainEvents(): void {
        this._domainEvents = [];
    }

    protected checkRule(rule: IBusinessRule): void {
        const isBroken = rule.isBroken();
        if (isBroken) {
            throw new BusinessRuleValidationError(rule);
        }
    }

    protected async checkRuleAsync(rule: IBusinessRuleAsync): Promise<void> {
        const isBroken = await rule.isBroken();
        if (isBroken) {
            throw new BusinessRuleValidationError(rule);
        }
    }

    equals(other: Entity): boolean {
        return other.id.equals(this.id);
    }
}