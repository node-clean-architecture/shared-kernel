export interface ISuccessResult<TData> {
    value: TData;
    error?: undefined;
}

export interface IErrorResult {
    value?: undefined;
    error: IError;
}

export interface IError {
    message: string;
    code: string;
    additionalData?: Record<string, unknown>;
}

export type IResult<TData> = ISuccessResult<TData> | IErrorResult;