import { IErrorResult, ISuccessResult } from "./IResult";

export class Result {
    private constructor() { /** */ }

    static success<TData>(value: TData): ISuccessResult<TData> {
        return {
            value,
        };
    }

    static error(code: string, message?: string, additionalData?: Record<string, string>): IErrorResult {
        return {
            error: {
                code,
                message: message || `${code}`,
                additionalData,
            },
        };
    }

    static empty(): ISuccessResult<void> {
        return {
            value: undefined,
        };
    }
}