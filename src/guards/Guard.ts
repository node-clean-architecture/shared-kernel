import { Optional } from "typescript-helper-types";

import { InvalidArgumentError } from "../errors";

// eslint-disable-next-line @typescript-eslint/no-namespace
export namespace Guard {
    export class Against {
        static undefined<T>(value: Optional<T>, message = "Value is not defined"): asserts value is T {
            if (value === null || value === undefined) {
                throw new InvalidArgumentError(message);
            }
        }

        static undefinedOrWhitespace(value: Optional<string>, message = "Value is not defined or is only whitespace"): asserts value is string {
            Guard.Against.undefined(value);

            if (value.trim().length === 0) {
                throw new InvalidArgumentError(message);
            }
        }

        static zero(value: number, message = "Value is 0"): void {
            if (value === 0) {
                throw new InvalidArgumentError(message);
            }
        }

        static negative(value: number, message = "Value is negative"): void {
            if (value < 0) {
                throw new InvalidArgumentError(message);
            }
        }

        static outOfRange(value: number, min: Optional<number>, max: Optional<number>, message = "Value is out of range"): void {
            if ((Guard.Against.notDefined(min) && value < min) || (Guard.Against.notDefined(max) && value > max)) {
                throw new InvalidArgumentError(message);
            }
        }

        private static notDefined<T>(value: Optional<T>): value is T {
            return value !== undefined && value !== null;
        }
    }
}
