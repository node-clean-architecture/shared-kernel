import { UniqueId } from "./UniqueId";

export abstract class DomainEvent {
    id = new UniqueId();
    dateOccurred = new Date();

    constructor(readonly name: string) {
    }
}