export abstract class ValueObject<TValue> {
    protected constructor(readonly value: TValue) { }

    equals(other: ValueObject<TValue>): boolean {
        return other.value === this.value;
    }
}