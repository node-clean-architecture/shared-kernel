import { DomainEvent } from "./DomainEvent";

export abstract class DomainEventHandler<TEvent extends DomainEvent> {
    constructor(readonly name: string) {
    }

    abstract handle(event: TEvent): Promise<void>;
}