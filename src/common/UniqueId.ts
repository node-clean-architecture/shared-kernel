import { v4 as createUuidV4 } from "uuid";

import { ValueObject } from "./ValueObject";

export class UniqueId extends ValueObject<string | number> {
    constructor(value?: string) {
        super(value || createUuidV4());
    }
}