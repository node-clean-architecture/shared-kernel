import { UniqueId } from "../../common/UniqueId";
import { Entity } from "../../Entity";
import { TestDomainEvent } from "./TestDomainEvent";

export class TestEntity extends Entity {
    constructor(id: string) {
        super();

        this.id = new UniqueId(id);
    }

    doWork(): void {
        this.addDomainEvent(new TestDomainEvent());
    }

    reset(): void {
        this.clearDomainEvents();
    }
}
