import { DomainEvent } from "../../common";

export class TestDomainEvent extends DomainEvent {
    constructor() {
        super(TestDomainEvent.name);
    }
}
