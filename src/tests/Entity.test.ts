import { TestDomainEvent, TestEntity } from "./utils";

describe("Entity tests", () => {
    it("should indicate equality", () => {
        const id = "test";
        expect(new TestEntity(id).equals(new TestEntity(id))).toEqual(true);
    });

    it("should not indicate equality", () => {
        const id = "test";
        const otherId = "test2";
        expect(new TestEntity(id).equals(new TestEntity(otherId))).toEqual(false);
    });

    describe('events', () => {
        const entity = new TestEntity("test");
        
        afterEach(() => entity.reset());

        it("should not have events by default", () => {
            expect(entity.domainEvents).toHaveLength(0);
        });

        it("should add a domain event", () => {
            entity.doWork();

            expect(entity.domainEvents).toHaveLength(1);
            expect(entity.domainEvents[0].name).toEqual(TestDomainEvent.name);
        });
    });
});