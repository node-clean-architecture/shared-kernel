import { Result } from "../result/Result";

describe('Result tests', () => {
    it('should create a success result', () => {
        const value = "test";
        const result = Result.success(value);
        expect(result.error).toEqual(undefined);
        expect(result.value).toEqual(value);
    });

    it('should create an error result', () => {
        const code = "code";
        const message = "message";
        const result = Result.error(code, message, {});

        expect(result.value).toEqual(undefined);
        expect(result.error).toStrictEqual({
            code,
            message,
            additionalData: {},
        });
    });

    it('should create an error result and fallback to code if no message is provided', () => {
        const code = "code";
        const result = Result.error(code, undefined, {});

        expect(result.value).toEqual(undefined);
        expect(result.error).toStrictEqual({
            code,
            message: code,
            additionalData: {},
        });
    });

    it('should create an empty result', () => {
        const result = Result.empty();
        expect(result.error).toEqual(undefined);
        expect(result.value).toEqual(undefined);
    });
});