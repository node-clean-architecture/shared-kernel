import { IBusinessRule, IBusinessRuleAsync } from "./IBusinessRule";

export class BusinessRuleValidationError extends Error {
    constructor(readonly brokenRule: IBusinessRule | IBusinessRuleAsync) {
        super(brokenRule.message);

        this.name = brokenRule.name;

        Error.captureStackTrace(this, BusinessRuleValidationError);
    }
}
