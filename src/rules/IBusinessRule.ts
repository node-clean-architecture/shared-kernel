export interface IBusinessRuleBase {
    readonly name: string;
    readonly message: string;
}

export interface IBusinessRule extends IBusinessRuleBase {
    isBroken(): boolean;
}

export interface IBusinessRuleAsync extends IBusinessRuleBase {
    isBroken(): Promise<boolean>;
}
