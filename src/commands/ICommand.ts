import { UniqueId } from "../common";

export interface ICommand {
    id: UniqueId;
}
