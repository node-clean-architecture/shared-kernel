import { UniqueId } from "../common";

export abstract class CommandBase {
    id: UniqueId = new UniqueId();
}
