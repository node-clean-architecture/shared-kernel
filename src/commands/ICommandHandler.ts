import { IResult } from "../result";
import { ICommand } from "./ICommand";

export interface ICommandHandler<TCommand extends ICommand, TCommandResult = void> {
    handle(command: TCommand): Promise<IResult<TCommandResult>>;
}
