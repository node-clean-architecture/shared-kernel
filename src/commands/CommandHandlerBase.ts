import { IResult } from "../result";
import { ICommand } from "./ICommand";
import { ICommandHandler } from "./ICommandHandler";

export abstract class CommandHandlerBase<TCommand extends ICommand, TCommandResult = void> implements ICommandHandler<TCommand, TCommandResult> {
    abstract handle(command: TCommand): Promise<IResult<TCommandResult>>;
}
